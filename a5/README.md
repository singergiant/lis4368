# LIS 4368 - Advanced Web Application Development

## Noah Singer

### Assignment 5 Requirements:

*Deliverables:*

1. Include Assignment 4 server-side validation and form
2. screenshots of pre-, post-valid user entry form
3. Provide Bitbucket read-only access to lis4368 repo, include links to the other assignment repos you created in README.md, using Markdown syntax 
4. A5 Bitbucket repo

#### README.md file should include the following items:

* pre-valid user entry form
* post-valid user entry form
* associated database entry

#### Assignment Screenshots:

*Screenshot of pre form validation*:

![pre form validation](img/pre.png)

*Screenshot of post form validation*:

![post form validation](img/post.png)

*Screenshot of associated database entry*:

![associated database entry](img/database.png)
<br></br>

#### Assignment Skillsets:

*Skillset 13*:
![Skillset 13](img/SS13.png)

*Skillset 14*:
![Skillset 14](img/SS14.png)

*Skillset 15*:
![Skillset 15](img/SS15.png)

