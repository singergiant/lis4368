# LIS 4368 - Advanced Web Application Development

## Noah Singer

### Assignment 3 Requirements:

*Deliverables:*

1. Entity Relationship Diagram (ERD)
2. Include Data (atleast 10 records each table)
3. Provide Bitbucket read-only access to A3 repo (Language SQL) *must* include README.md using markdown syntax , and include links to *all* of the following files(from README.md):
   - docs folder: a3.mwb and a3.sql
   - img folder: a3.png (export a3.mwb file as a3.png)
   - README.md (*MUST* display a3.png ERD)
4. A3 Bitbucket repo      
<br></br>

> #### README.md file should include the following items:

* Screenshot of A3 ERD that links to the img    
* A3 docs: a3.mwb and a3.sql
<br></br>

> #### Assignment Screenshots and links:

*Screenshot of A3 ERD*:

![A3 ERD](img/a3_ERD.png "ERD based upon A3 Requirements")

*Screenshot of pet table*:

![pet table](img/pettable.png "Screenshot of pet table")

*Screenshot of petstore table*:

![petstore table](img/petstoretable.png "Screenshot of petstore table")

*Screenshot of customer table*:

![customer table](img/customertable.png "Screenshot of customer table")

*A3 docs: a3.mwb and a3.sql:l*:

[A3 MWB File](docs/A3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")

<br></br>
> #### Assignment Skillsets (4-6):

*Skillset 4: Directory Info*:
[![SS4](img/SS4.png)](C:\Development\tomcat\webapps\lis4368\skillsets\Q4_Directory_Info)

*Skillset 5: Character Info*:
[![SS5](img/SS5.png)](C:\Development\tomcat\webapps\lis4368\skillsets\Q5_Character_Info)

*Skillset 6: Determine Character*:
[![SS6](img/SS6.png)](C:\Development\tomcat\webapps\lis4368\skillsets\Q6_Determine_Character)