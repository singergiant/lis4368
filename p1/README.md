# LIS 4368 - Advanced Web Application Development

## Noah Singer

### Project 1 Requirements:

*Deliverables:*

1. Failed form validation screenshot
2. Passed form validation screenshot
3. Bitbucket read-only access to lis4368 repo
4. Link to Bitbucket repo
5. Image carousel on LIS4368 portal (Main/Splash Page)
<br>

        //https://fontawesome.com/
        //fontawesome is an icon toolkit based on CSS
        //the following values assign icons to the validation state:

        valid: 'fa fa-check': A checkmark when the values are valid
        invalid: 'fa fa-times': A times symbol (X) when form is invalid
        validating: 'fa fa-refresh: A refresh symbol while validating

> #### Assignment Screenshots and links:
<br>

*Screenshot of failed validation*:
![Failed form validation](img/failed.png "Failed form validation")

*Screenshot of passed validation*:
![Passed form validation](img/passed.png "Passed form validation")

<br></br>
> #### Assignment Skillsets (4-6):
 <br>

*Skillset 7: Count Characters*:
[![SS7](img/SS7.png)](C:\Development\tomcat\webapps\lis4368\skillsets\Q7_Count_Characters)

*Skillset 8: ASCII*:
[![SS8_1](img/SS8_1.png)](C:\Development\tomcat\webapps\lis4368\skillsets\Q8_ASCII)
[![SS8_2](img/SS8_2.png)](C:\Development\tomcat\webapps\lis4368\skillsets\Q8_ASCII)
[![SS8_3](img/SS8_3.png)](C:\Development\tomcat\webapps\lis4368\skillsets\Q8_ASCII)

*Skillset 9: Grade Calculator*:
[![SS9](img/SS9.png)](C:\Development\tomcat\webapps\lis4368\skillsets\Q9_Grade_Calculator)