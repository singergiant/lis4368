> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Application Development

## Noah Singer

### Assignment 2 Requirements:

*Three Parts*

1. Describing dynamic websites
2. Creating database and compiling a Database Servlet 
3. Questions
<br></br>
> #### README.md file should include the following items:

* Screenshot of http://localhost:9999/hello
* Screenshot of http://localhost:9999/hello/HelloHome.html
* Screenshot of http://localhost:9999/hello/sayhello
* Screenshot of http://localhost:9999/hello/querybook.html
* Screenshot of [querybook.html](http://localhost:9999/hello/querybook.html)
* Screenshot of [the query results](http://localhost:9999/hello/query?author=Tan+Ah+Teck)
* Screenshot of [a2/index.jsp](http://localhost:9999/lis4368/a2/index.jsp)
<br></br>
> #### Assignment Screenshots:

*Screenshot of http://localhost:9999/hello*:

![http://localhost:9999/hello](img/hello.png)

*Screenshot of http://localhost:9999/hello/HelloHome.html*:

![http://localhost:9999/hello/HelloHome.html](img/indexhtml.png)

*Screenshot of http://localhost:9999/hello/sayhello*:

![http://localhost:9999/hello/sayhello](img/sayhello.png)

*Screenshot of querybook.html*:

![http://localhost:9999/hello/querybook.html](img/querybook.png)

*Screenshot of the query results*:

![http://localhost:9999/hello/query?author=Tan+Ah+Teck](img/queryresult.png)

*Screenshot of a2/index.jsp*:

![http://localhost:9999/lis4368/a2/index.jsp](img/a2indexjsp.png)


> #### Tutorial Links:

*Java Servlet Tutorial:*
[Java Servlet Tutorial Link](https://personal.ntu.edu.sg/ehchua/programming/howto/Tomcat_HowTo.html#zz-2.8 "Java Servlet Tutorial")

<br></br>
> #### Skillsets:

*Skillset 1*:  

[![SS1](img/SS1.png)](C:\Development\tomcat\webapps\lis4368\skillsets\Q1_System_Properties)

*Skillset 2*:
[![SS2](img/SS2.png)](C:\Development\tomcat\webapps\lis4368\skillsets\Q2_Looping_Structures)

*Skillset 3*:

[![SS3](img/SS3.png)](C:\Development\tomcat\webapps\lis4368\skillsets\Q3_Number_Swap)

