# LIS 4368 - Advanced Web Application Development

## Noah Singer

### Project 2 Requirements:

*Deliverables:*

1. Include Assignment 4 server-side validation and form
2. screenshots of pre-, post-valid user entry form, update form, delete form warning, and the associated database.
3. Provide Bitbucket read-only access to lis4368 repo, include links to the other assignment repos you created in README.md, using Markdown syntax 
4. A5 Bitbucket repo

#### README.md file should include the following items:

* pre-valid user entry form
* post-valid user entry form
* associated database entry
* update customer form
* delete entry warning

#### Assignment Screenshots:

*Screenshot of valid userform entry*:

![valid userform entry](img/valid.png)

*Screenshot of post form passed validation*:

![post form validation](img/passed.png)

*Screenshot of costumers.jsp data displayed in the webapp*:

![customers.jsp data](img/data.png)

*Screenshot of the modify/update form*:

![Update Customer form](img/modify.png)

*Screenshot of costumers.jsp displayed with modified data*:

![customers.jsp modified data](img/mod.png)

*Screenshot of the delete warning (customer.jsp)*:

![costumer.jsp delete warning](img/delete.png)

*Associated database with changed (select, insert, update, delete)*:

![customers.jsp data](img/database.png)
<br></br>
