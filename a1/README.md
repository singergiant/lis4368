# LIS 4368 - Advanced Web Application Development

## Noah Singer

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Ch 1 - 4)

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running http://localhost:9999
* git commads with short descriptions;
* Bitbucket repo links: a) this assingment and b) the completed tutorial (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init: create a new local repository
2. get status: lists the files that have been modified and still need to be added or commited
3. get add: add files to the stging index
4. git commit: commit any changes in the staged area
5. git push: send changes to the master branch of the remote repo
6. git pull: fetch the changes of the remote repo to the local directory
7. git checkout: switch from one branch to another 

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![running java Hello](img/jdk_install.png)

*Screenshot of running http://localhost:9999*:

![http://localhost:999](img/tomcat.png)

*Screenshot of A1/index.jsp*:

![A1/index.jsp](img/a1_index_jsp.png)


#### Bitbucket Repo Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/singergiant/bitbucketstationlocations/ "Bitbucket Station Locations")

*Assignment 1*:
[A1 Repository](https://bitbucket.org/singergiant/lis4368/src/master/a1/ "Assignment 1 Repo")
