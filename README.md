> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4368: Advanced Web Application Development

## Noah A. Singer

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Download and install AMPPS
    - Download and Install JDK
    - Download and install Apache Tomcat
    - Clone assignment starter files
    - Development and deploy Java Hello webapp
    - Create Bitbucket repo
    - Provide screenshots of installations/webapp
    - Complete Bitbucket tutorials (bitbucketstationlocations)
    - Provide git command descriptions
    <br></br>
    
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create local MySQL connection
    - Create eBookShop database
    - Write a 'Hello-World' Java servlet
    - Write a database servlet
    - Set environmental variable paths
    - Compile servlet files
    - Provide screenshots of running webapp servlet
    <br></br>

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Understanding database business rules
    - Create ERD with inserts
    - Forward engineering and generating SQL script
    - Provide screenshot of ERD
    - Provide .mwb and .sql files
    <br></br>

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Model View Controller (MVC) overview
    - Compile servlet files/implement server-side validation
    - Complete A4 webapp form
    <br></br>

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Include server-side validation from A4
    - Compile servlet files 
    - Display screenshots of pre-, post-valid user entry form
    - Display screenshots of MySQL table
    <br></br>

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Complete LIS4368 Splash page carousel
    - Add form controls to match attributes of 'customer' table
    - Create client side validation wth jQuery and regexp
    <br></br>

7. [P2 README.md](p2/README.md "My P1 README.md file")
    - Putting it all together 
    - Add CRUD functionality
    - Compile servlet files/include server-side validation
    - (Optional) Add search (SCRUD) option
    <br></br>
